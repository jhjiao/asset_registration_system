import csv

with open('assets.csv', 'a', newline='') as csvfile:

    writer = csv.writer(csvfile, dialect='excel')
    header=['Neighborhood name', 'Address', 'Year of Construction', 'Building', 'Unit', 'Orientation (North/South/West/East)', 'Area']
    writer.writerow(header)

title=input('Enter neighbourhood name: ')
address = input('Enter street address: ')
year = input('Enter year of construction: ')

block_loop = True
while block_loop:
    block = input('Enter the building number: ')
    start_floor = input('Enter the lowest level：')
    end_floor = input('Enter the highest storey：')

    input('You are about to enter information that is tailored to a specific unit; press "Enter" to continue')
    start_floor_units = {}
    floor_last_number = []

    unit_loop = True
    while unit_loop:
        last_number = input('Enter tail digits of the unit number excluding the left-most digit: ')
        floor_last_number.append(last_number)
        unit_number = int(start_floor + last_number)
        #explanation: unit_number = start_floor + last_number, i.e. '3' + '01' = '301'

        direction = int(input('Enter the orientation of %d (1-N/S, 2-E/W)：' % unit_number ))
        area = int(input('Enter the area of %d in ㎡ ：' % unit_number))
        start_floor_units[unit_number] = [direction,area]

        continued = input('Would you like to continue entering the next tail unit number? (Yes/No)：')
        if continued == 'No':
            unit_loop = False

        else:
            unit_loop = True       

    unit_units = {}
    unit_units[start_floor] = start_floor_units

    for floor in range(int(start_floor) + 1, int(end_floor) + 1):
        floor_units = {}
        for i in range(len(start_floor_units)):
            number = str(floor) + floor_last_number[i]
            info = start_floor_units[int(start_floor + floor_last_number[i])]
            floor_units[int(number)] = info
        unit_units[floor] = floor_units

    with open('assets.csv', 'a', newline='')as csvfile:
        writer = csv.writer(csvfile, dialect='excel')
        for sub_dict in unit_units.values():
            for unit,info in sub_dict.items():
                dire = ['', 'South/North', 'East/West']
                writer.writerow([title,address,year,block, unit,dire[info[0]],info[1]])   
   
    block_continue = input('Do you want to enter another building？(Yes/No)：')
    if block_continue == 'No':
        block_loop = False
    else:
        block_loop = True

print('Congratulations, you have completed your assigned task of registering the assets！')    